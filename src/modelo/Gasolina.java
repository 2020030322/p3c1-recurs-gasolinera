package modelo;

public class Gasolina {
        private String idGasolina;
        private String Tipo;
        private float Precio;
        //CONSTRUCTORES
        public Gasolina(){
           this.Precio = 0;
           this.idGasolina = "";
           this.Tipo = "";
        }
        public Gasolina(String idGasolina, String Tipo, float Precio){
            this.idGasolina = idGasolina;
            this.Tipo = Tipo;
            this.Precio = Precio;
        }
        public Gasolina(Gasolina GS){
            this.idGasolina = GS.idGasolina;
            this.Tipo = GS.Tipo;
            this.Precio = GS.Precio;
        }
        //METODOS SETT & GET

    public String getIdGasolina() {
        return idGasolina;
    }

    public void setIdGasolina(String idGasolina) {
        this.idGasolina = idGasolina;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public float getPrecio() {
        return Precio;
    }

    public void setPrecio(float Precio) {
        this.Precio = Precio;
    }
}
