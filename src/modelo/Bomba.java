package modelo;

public class Bomba {
    private int numBomba;
    private float cpBomba;
    private float acumLitros;
    private Gasolina gasolina;
    //CONSTRUCTORES
    public Bomba(){
        this.numBomba = 0;
        this.cpBomba = 0;
        this.acumLitros = 0;
        this.gasolina = new Gasolina();
    }
    public Bomba(int numBomba,float cpBomba,float acumLitros,Gasolina gasolina){
        this.numBomba = numBomba;
        this.cpBomba = cpBomba;
        this.acumLitros = acumLitros;
        this.gasolina = gasolina;
    }
    public Bomba(Bomba BOOM){
        this.numBomba = BOOM.numBomba;
        this.cpBomba = BOOM.cpBomba;
        this.acumLitros = BOOM.acumLitros;
        this.gasolina = BOOM.gasolina;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCpBomba() {
        return cpBomba;
    }

    public void setCpBomba(float cpBomba) {
        this.cpBomba = cpBomba;
    }

    public float getAcumLitros() {
        return acumLitros;
    }

    public void setAcumLitros(float acumLitros) {
        this.acumLitros = acumLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }    
    public void IniciarBomba(int idBomba,Gasolina gasolina){
        this.gasolina = gasolina;
        this.cpBomba = 200.0f;
        this.acumLitros = 0.0f;
        this.numBomba = idBomba;
    }
    public float InventarioGasolina(){
        return cpBomba - acumLitros;
    }
    public float VenderGasolina(float cantidad){
        this.acumLitros += cantidad;
        return cantidad * this.gasolina.getPrecio();
    }
    public float VentasTotales(){
        return this.acumLitros * this.gasolina.getPrecio();
    }
}
