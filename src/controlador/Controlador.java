
package controlador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Bomba;
import modelo.Gasolina;
import vista.dlgSEDENA;

public class Controlador implements ActionListener {
private Gasolina gas;
private Bomba boom;
private dlgSEDENA vista;

public Controlador(Bomba boom,Gasolina gas,dlgSEDENA vista){
    this.boom = boom;
    this.gas = gas;
    this.vista = vista;
    vista.btnCerrar.addActionListener(this);
    vista.btnIniciarBomba.addActionListener(this);
    vista.btnRegistrar.addActionListener(this);
    vista.cbTTipo.addActionListener(this);
    
}
private void IniciarVista(){
        vista.setTitle("...:::Gasolinera Petroil:::...");
        //Esta linea de codigo es para modifica el (Width,Heigth)
        vista.setSize(610,450);
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
}
private void LimpiarTexto(){
    vista.txtTotalVenta.setText("");
    vista.txtCosto.setText("");
    vista.txtCantidad.setText("");
}

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.cbTTipo) {
            vista.btnIniciarBomba.setEnabled(true);
        }
        if(e.getSource()==vista.btnIniciarBomba){
            gas.setIdGasolina(String.valueOf(vista.cbTTipo.getSelectedIndex()));
            gas.setTipo(vista.cbTTipo.getSelectedItem().toString());
            gas.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
            
            try{
                LimpiarTexto();
                boom.IniciarBomba(Integer.parseInt(vista.txtNumBomba.getText()), gas);
                vista.sliCantidadBomba.setMaximum(200);
                vista.sliCantidadBomba.setValue(200);
                vista.txtContadorVentas.setText(String.valueOf(0));
                vista.btnRegistrar.setEnabled(true);
            }catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error : " + ex2.getMessage());
            }
        }

        if(e.getSource()==vista.btnRegistrar){
            int contador = Integer.parseInt(vista.txtContadorVentas.getText());
            try{
                float cantidad = Float.parseFloat(vista.txtCantidad.getText());
                if(cantidad <=boom.InventarioGasolina()){
                    vista.txtCosto.setText(String.valueOf(boom.VenderGasolina(cantidad)));
                    vista.txtTotalVenta.setText(String.valueOf(boom.VentasTotales()));
                    contador ++;
                    vista.txtContadorVentas.setText(String.valueOf(contador));
                    vista.sliCantidadBomba.setValue((int)boom.InventarioGasolina());
                }
                else {
                    JOptionPane.showMessageDialog(vista,"Cantidad excedida");
                }
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista,"Surgio el seguiente error:"+ ex.getMessage());
            }
        }
        
        if(e.getSource()==vista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(vista,"Seguro que quieres salir",
                    "Decide",JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_NO_OPTION){
                vista.dispose();
                System.exit(0);
            }
        }
    }
    
    public static void main(String[] args) {
        Bomba boom = new Bomba();
        Gasolina gas = new Gasolina();
        dlgSEDENA vista = new dlgSEDENA(new JFrame(),true);
        Controlador control = new Controlador(boom,gas,vista);
        control.IniciarVista();
    }
}
